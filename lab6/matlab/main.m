clear;
IMG_PATH = '../doc/img/generated/';


%% Initial params
% Plant

theta = [7;
        -7];

% Reference

lambda = 1;


%% Modeling
gamma = 1;
x_init = 0;

sim_time = 200;
simulation.simulateModel('isModelSimulationOn', false,...
                         'isPrintingEnable',    false,...
                         'plotSavePath',        IMG_PATH,...
                         'showPlots',           false);