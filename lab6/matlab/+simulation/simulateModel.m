function [J_out] = simulateModel(varargin)
    optionalArgsProc = {...
        @(p)addParameter(p,   'showPlots',           true);
        @(p)addParameter(p,   'isModelSimulationOn', true);
        @(p)addParameter(p,   'isPrintingEnable',    false);
        @(p)addParameter(p,   'plotSavePath',        './', @(s)ischar(s));
        @(p)addParameter(p,   'prefix',              '',   @(s)ischar(s));
    };
    args = helptools.parseArgs(varargin, optionalArgsProc);


    %% Simulation and ploting
    buildGraph_ = @helptools.buildGraph;

    J_out = struct();
    if args.isModelSimulationOn
        sim('models/model');
        
        dataset = theta_tilde_dataset;

        theta_figure = buildGraph_('', dataset.time, dataset.signals.values(:,1),...
                               'xLabel',    '$$t, s$$',...
                               'yLabel',    '$$\tilde\theta_1(t)$$',...
                               'showFig',   args.showPlots);
                           
        buildGraph_('', dataset.time, dataset.signals.values(:,2),...
                               'xLabel',    '$$t, s$$',...
                               'yLabel',    '$$\tilde\theta_2(t)$$',...
                               'showFig',   args.showPlots,...
                               'isNewFig',  false);
                          
        dataset = u_dataset;

        u_figure = buildGraph_('', dataset.time, dataset.signals.values,...
                               'xLabel',    '$$t, s$$',...
                               'yLabel',    '$$u(t)$$',...
                               'showFig',   args.showPlots);
        
        dataset = epsilon_dataset;
        
        epsilon_figure = buildGraph_('', dataset.time, dataset.signals.values,...
                               'xLabel',    '$$t, s$$',...
                               'yLabel',    '$$\varepsilon(t)$$',...
                               'showFig',   args.showPlots);

        if args.isPrintingEnable
            fullPath = sprintf('%stheta%s.eps', args.plotSavePath, args.prefix);
            print(theta_figure, fullPath, '-depsc');

            fullPath = sprintf('%su%s.eps', args.plotSavePath, args.prefix);
            print(u_figure, fullPath, '-depsc');
            
            fullPath = sprintf('%sepsilon%s.eps', args.plotSavePath, args.prefix);
            print(epsilon_figure, fullPath, '-depsc');
        end
    end 
end