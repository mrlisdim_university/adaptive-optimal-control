(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      1546,         53]
NotebookOptionsPosition[      1136,         38]
NotebookOutlinePosition[      1505,         54]
CellTagsIndexPosition[      1462,         51]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"Clear", "[", "\"\<Global`*\>\"", "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.71706853601853*^9, 
  3.717068561311849*^9}},ExpressionUUID->"ce2c3eba-66f1-45cd-9717-\
6b610b092774"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"ExtractFirst", "[", "x_", "]"}], ":=", 
   RowBox[{"First", "[", 
    RowBox[{"Flatten", "[", "x", "]"}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7170769944659567`*^9, 3.717077032852512*^9}, {
  3.717077275848681*^9, 
  3.717077314054058*^9}},ExpressionUUID->"af6a3ea9-9df2-4a74-a4bc-\
e81f0f9d24dd"]
},
WindowSize->{1920, 997},
WindowMargins->{{0, Automatic}, {Automatic, 37}},
Magnification:>1.5 Inherited,
FrontEndVersion->"11.2 for Linux x86 (64-bit) (September 10, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 216, 5, 75, "Input",ExpressionUUID->"ce2c3eba-66f1-45cd-9717-6b610b092774"],
Cell[777, 27, 355, 9, 72, "Input",ExpressionUUID->"af6a3ea9-9df2-4a74-a4bc-e81f0f9d24dd"]
}
]
*)

