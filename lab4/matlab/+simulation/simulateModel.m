function [J] = simulateModel(varargin)
    optionalArgsProc = {...
        @(p)addParameter(p,   'showPlots',           true);
        @(p)addParameter(p,   'isModelSimulationOn', true);
        @(p)addParameter(p,   'isPrintingEnable',    false);
        @(p)addParameter(p,   'plotSavePath',        './', @(s)ischar(s));
        @(p)addParameter(p,   'prefix',              '',   @(s)ischar(s));
    };
    args = helptools.parseArgs(varargin, optionalArgsProc);


    %% Simulation and ploting
    buildGraph_ = @helptools.buildGraph;

    J = struct();
    if args.isModelSimulationOn
        sim('models/model');
        
        dataset = J_dataset;
        J = J_dataset;

        J_figure = buildGraph_('', dataset.time, dataset.signals.values,...
                               'xLabel',    '$$t, s$$',...
                               'yLabel',    '$$J(t)$$',...
                               'showFig',   args.showPlots);
                           
        dataset = e_dataset;

        e_figure = buildGraph_('', dataset.time, dataset.signals.values(:,1),...
                               'xLabel',    '$$t, s$$',...
                               'yLabel',    '$$e_1(t)$$',...
                               'showFig',   args.showPlots);     
                           
        buildGraph_('', dataset.time, dataset.signals.values(:,2),...
                               'xLabel',    '$$t, s$$',...
                               'yLabel',    '$$e_2(t)$$',...
                               'showFig',   args.showPlots,...
                               'isNewFig',  false);   

        if args.isPrintingEnable
            fullPath = sprintf('%sJ%s.eps', args.plotSavePath, args.prefix);
            print(J_figure, fullPath, '-depsc');
            fullPath = sprintf('%se%s.eps', args.plotSavePath, args.prefix);
            print(e_figure, fullPath, '-depsc');
        end
    end 
end