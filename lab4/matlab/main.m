clear;
IMG_PATH = '../doc/img/generated/';


%% Initial params
%# Plant
A = [0  1;
     0 -1];

b = [3 4]';

C = [1 0];

x_init = [1 0]';

n = length(A);

%# Riccati equation
G = eye(n);
 
W = [6 3;
     3 2];
 
R = chol(W);

V = 5;


%% Finding L
[P,~,~] = care(A, C', G*W*G', V);
L = P * C' * V^(-1);


%% Modeling original
sim_time = 500;

J = simulation.simulateModel('isModelSimulationOn', false,...
                             'isPrintingEnable',    false,...
                             'plotSavePath',        IMG_PATH,...
                             'showPlots',           true);
if ~helptools.isEmptyStruct(J)
    fprintf('J_est = %f\n', J.signals.values(end));
end
%% Modeling with L shifted
L_shift = 0.05;
L = L + L_shift;

J = simulation.simulateModel('isModelSimulationOn', false,...
                             'isPrintingEnable',    false,...
                             'plotSavePath',        IMG_PATH,...
                             'showPlots',           true,...
                             'prefix',              '_shifted_L');
if ~helptools.isEmptyStruct(J)
    fprintf('J_est_L = %f\n', J.signals.values(end));
end                         
%% Modeling with W shifted
W_shift = 0.1;
W = W + W_shift;

J = simulation.simulateModel('isModelSimulationOn', false,...
                             'isPrintingEnable',    false,...
                             'plotSavePath',        IMG_PATH,...
                             'showPlots',           true,...
                             'prefix',              '_shifted_W');      
if ~helptools.isEmptyStruct(J)
    fprintf('J_est_W = %f\n', J.signals.values(end));
end                         
%% Modeling with V shifted
V_shift = 0.1;
V = V + V_shift;

J = simulation.simulateModel('isModelSimulationOn', false,...
                             'isPrintingEnable',    false,...
                             'plotSavePath',        IMG_PATH,...
                             'showPlots',           true,...
                             'prefix',              '_shifted_V');   
if ~helptools.isEmptyStruct(J)
    fprintf('J_est_V = %f\n', J.signals.values(end));
end                         