clear;
IMG_PATH = '../doc/img/generated/';


%% Initial params
% Plant

A = [0 1;
     1 1];

b = [0 1]';

% Riccati equation
Q = [1 1;
     1 4];

r = 1;


%% Finding optimal K
[P,~,~] = care(A, b, Q, r);
K = r^-1 * b' * P;


%% Modeling
x_init = [1 0]';
sim_time = 10;
J = simulation.simulateModel('isModelSimulationOn', false,...
                             'isPrintingEnable',    false,...
                             'plotSavePath',        IMG_PATH,...
                             'showPlots',           false);
if ~utils.isEmptyStruct(J)
    fprintf('J_est = %f\n',...
            J.signals.values(length(J.signals.values)));
end


%% Modeling for K shifted
K_shift = [0.1 0.1];
K = K + K_shift;

prefix = '_shifted';
J = simulation.simulateModel('isModelSimulationOn', false,...
                             'isPrintingEnable',    false,...
                             'plotSavePath',        IMG_PATH,...
                             'showPlots',           false,...
                             'prefix',              prefix);
if ~utils.isEmptyStruct(J)
    fprintf('J_est%s = %f\n', prefix,...
             J.signals.values(length(J.signals.values)));
end
