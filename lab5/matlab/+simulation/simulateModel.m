function [J_out] = simulateModel(varargin)
    optionalArgsProc = {...
        @(p)addParameter(p,   'showPlots',           true);
        @(p)addParameter(p,   'isModelSimulationOn', true);
        @(p)addParameter(p,   'isPrintingEnable',    false);
        @(p)addParameter(p,   'plotSavePath',        './', @(s)ischar(s));
        @(p)addParameter(p,   'prefix',              '',   @(s)ischar(s));
    };
    args = helptools.parseArgs(varargin, optionalArgsProc);


    %% Simulation and ploting
    buildGraph_ = @helptools.buildGraph;

    J_out = struct();
    if args.isModelSimulationOn
        sim('models/model');
        
        dataset = x_dataset;

        x_figure = buildGraph_('', dataset.time, dataset.signals.values(:,1),...
                               'xLabel',    '$$t, s$$',...
                               'yLabel',    '$$x_1(t)$$',...
                               'showFig',   args.showPlots);
                           
        buildGraph_('', dataset.time, dataset.signals.values(:,2),...
                               'xLabel',    '$$t, s$$',...
                               'yLabel',    '$$x_2(t)$$',...
                               'showFig',   args.showPlots,...
                               'isNewFig',  false);
                          
        dataset = u_dataset;

        u_figure = buildGraph_('', dataset.time, dataset.signals.values,...
                               'xLabel',    '$$t, s$$',...
                               'yLabel',    '$$u(t)$$',...
                               'showFig',   args.showPlots);
        
        dataset = J_dataset;
        J_out = J_dataset;
        
        J_figure = buildGraph_('', dataset.time, dataset.signals.values,...
                               'xLabel',    '$$t, s$$',...
                               'yLabel',    '$$J(t)$$',...
                               'showFig',   args.showPlots);

        if args.isPrintingEnable
            fullPath = sprintf('%sx%s.eps', args.plotSavePath, args.prefix);
            print(x_figure, fullPath, '-depsc');

            fullPath = sprintf('%su%s.eps', args.plotSavePath, args.prefix);
            print(u_figure, fullPath, '-depsc');
            
            fullPath = sprintf('%sJ%s.eps', args.plotSavePath, args.prefix);
            print(J_figure, fullPath, '-depsc');
        end
    end 
end