function args_parsed = parseArgs(args, addParamsFunc)
    p_ = inputParser;
    for i=1:length(addParamsFunc)
        addParamsFunc{i}(p_);
    end
    parse(p_, args{:})
    args_parsed = p_.Results;
end