function args_parsed = parseArgs(args, addParamsFuncs)
    p_ = inputParser;
    for i = 1:length(addParamsFuncs)
        addParamsFuncs{i}(p_);
    end
    parse(p_, args{:})
    args_parsed = p_.Results;
end