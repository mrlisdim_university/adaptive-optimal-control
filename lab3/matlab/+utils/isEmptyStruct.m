function [ isEmpty ] = isEmptyStruct( s )
    isEmpty = isempty(fieldnames(s));
end

