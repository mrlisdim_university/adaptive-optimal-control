clear;
IMG_PATH = '../doc/img/generated/';


%% Initial params
%# Plant
A = [0  1;
     0 -1];
 
b = [3 4]';

C = [1 0];

%# Riccati equation
Q = [2 0;
     0 1];
 
r = 5;

%# Generator
Gamma = [0  0 -6  0;
         0  0  0  4;
         6  0  0  0;
         0 -1  0  0];
H = [1 1 0 0];

gen_init = [1 0 0 1]';

%% Finding optimal K
[P,~,~] = care(A, b, Q, r);
K = r^-1 * b' * P;

%% Finding Lg
% from main.nb (Wolfram Mathematica)
Lg = [0.589146 0.611361 -1.98314 1.28401];

Mg = [1       1         0        0;
      1.15818 0.564706 -0.450402 1.31765];

%% Modeling for K optimal
sim_time = 2;

x_init = [1 0]';
[~,~,J] = simulation.simulateModel('isModelSimulationOn', false,...
                                   'isPrintingEnable',    false,...
                                   'plotSavePath',        IMG_PATH);

if ~utils.isEmptyStruct(J)
    fprintf('J_est = %f\n', J.signals.values(end));
end

%% Modeling for K shifted
%# Finding K shifted
K_shift = [0.1 0.1];
K = K + K_shift;

%# Finding Lg
% from main.nb (Wolfram Mathematica)
Lg = [0.804963 0.767832 -2.02818 1.41577];

%# Modeling
prefix = '_shifted';
[~,~,J] = simulation.simulateModel('isModelSimulationOn', false,...
                                   'isPrintingEnable',    false,...
                                   'plotSavePath',        IMG_PATH,...
                                   'prefix',              prefix);
if ~utils.isEmptyStruct(J)
    fprintf('J_est%s = %f\n', prefix,...
        J.signals.values(end));
end
