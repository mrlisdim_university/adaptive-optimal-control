echo off

set TOOLCHAIN_PATH=C:\JPG-PNGtoEPS\
set TOOLCHAIN=batchConvertAllPngToEps.bat

set PATH_LIST=(.\)

set ROOT=%CD%

FOR /d %%i IN %PATH_LIST% DO (	
	@echo %ROOT%\%%
	cd /D "%ROOT%\%%i"
	for %%F in (*.png) do COPY %%F %TOOLCHAIN_PATH%
	for %%F in (*.jpg) do COPY %%F %TOOLCHAIN_PATH%
	cd /D "%TOOLCHAIN_PATH%"
	call %TOOLCHAIN%
	for %%F in (*.eps) do COPY %%F "%ROOT%\%%i"
	for %%F in (*.png) do DEL %%F
	for %%F in (*.jpg) do DEL %%F
	for %%F in (*.eps) do DEL %%F
)