function [x_out, u_out, J_out] = simulateModel(varargin)
    % Parse args
    optionalArgsProc = {...
        @(p)addParameter(p,   'isModelSimulationOn', true);
        @(p)addParameter(p,   'isPrintingEnable',    false);
        @(p)addParameter(p,   'plotSavePath',        './');
        @(p)addParameter(p,   'prefix',              '', @(s)ischar(s));
    };
    args = utils.parseArgs(varargin, optionalArgsProc);

    % Proc simulation
    if args.isModelSimulationOn
        sim('models/model');
        
        dataset = x_dataset;
        x_out = x_dataset;

        config = struct();
        config.xy = [dataset.time dataset.signals.values(:,1)];
        config.xLabel = "$$t, s$$";
        config.yLabel = "$$x_1(t)$$";
        x_figure = buildGraph("", config, true);
        
        config = struct();
        config.xy = [dataset.time dataset.signals.values(:,2)];
        config.xLabel = "$$t, s$$";
        config.yLabel = "$$x_2(t)$$";
        buildGraph("", config, false);
        
        dataset = u_dataset;
        u_out = u_dataset;

        config = struct();
        config.xy = [dataset.time dataset.signals.values];
        config.xLabel = "$$t, s$$";
        config.yLabel = "$$u(t)$$";
        u_figure = buildGraph("", config, true);
        
        dataset = J_dataset;
        J_out = J_dataset;
        
        config = struct();
        config.xy = [dataset.time dataset.signals.values];
        config.xLabel = "$$t, s$$";
        config.yLabel = "$$J(t)$$";
        J_figure = buildGraph("", config, true);

        if args.isPrintingEnable
            fullPath = sprintf('%sx%s.eps', args.plotSavePath, args.prefix);
            print(x_figure, fullPath, '-depsc');

            fullPath = sprintf('%su%s.eps', args.plotSavePath, args.prefix);
            print(u_figure, fullPath, '-depsc');
            
            fullPath = sprintf('%sJ%s.eps', args.plotSavePath, args.prefix);
            print(J_figure, fullPath, '-depsc');
        end
    end 
end