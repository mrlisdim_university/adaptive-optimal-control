clear;
IMG_PATH = '../doc/img/generated/';


%% Initial params
% Plant
A = [0  1 
     0 -1];
b = [3
     4];

% Riccati equation
Q = [2 0
     0 1];
r = 5;


%% Found optimal K
[P,~,~] = care(A, b, Q, r);
K = r^-1 * b' * P;


%% Modeling
x_init = [1 0]';
sim_time = 10;
[~,~,J] = simulation.simulateModel('isModelSimulationOn', true,...
                                   'isPrintingEnable',    false,...
                                   'plotSavePath',        IMG_PATH);

fprintf('J_est = %f\n', J.signals.values(length(J.signals.values)));


%% Modeling for K shifted
K_shift = [0.1 0.1];
K = K + K_shift;

prefix = '_shifted';
[~,~,J] = simulation.simulateModel('isModelSimulationOn', true,...
                                   'isPrintingEnable',    true,...
                                   'plotSavePath',        IMG_PATH,...
                                   'prefix',              prefix);

fprintf('J_est%s = %f\n', prefix,...
        J.signals.values(length(J.signals.values)));
