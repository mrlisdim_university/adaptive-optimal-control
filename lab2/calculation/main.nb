(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      2181,         65]
NotebookOptionsPosition[      1644,         47]
NotebookOutlinePosition[      2013,         63]
CellTagsIndexPosition[      1970,         60]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "\"\<Global`*\>\"", "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.7218868316804237`*^9, 
  3.7218868644342847`*^9}},ExpressionUUID->"b965f2d4-5cfe-435f-b941-\
f369b21b3919"],

Cell[CellGroupData[{

Cell["\:0418\:0441\:0445\:043e\:0434\:043d\:044b\:0435 \:0434\:0430\:043d\
\:043d\:044b\:0435", "Chapter",
 CellChangeTimes->{{3.721884846351441*^9, 
  3.7218848491787415`*^9}},ExpressionUUID->"9a70a099-afd9-4d76-8ce3-\
71ea995685a5"],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.7218847208934994`*^9, 3.7218848069456964`*^9}, {
   3.72188515948744*^9, 3.72188520388947*^9}, {3.721885511612426*^9, 
   3.7218855123043957`*^9}, {3.7218859093869295`*^9, 
   3.7218859121434937`*^9}, {3.721886223809352*^9, 3.72188623024395*^9}, {
   3.721886294963842*^9, 3.7218863273179026`*^9}, {3.7218865693105927`*^9, 
   3.7218865721517525`*^9}, {3.721886809604185*^9, 3.7218868099452047`*^9}, {
   3.721894097204965*^9, 3.7218941031883044`*^9}, 
   3.721994524312648*^9},ExpressionUUID->"e5041401-a738-4cb6-8db1-\
332b0d65ab54"]
}, Open  ]]
},
WindowSize->{1920, 997},
WindowMargins->{{0, Automatic}, {Automatic, 37}},
Magnification:>1.3 Inherited,
FrontEndVersion->"11.2 for Linux x86 (64-bit) (September 10, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 224, 5, 64, "Input",ExpressionUUID->"b965f2d4-5cfe-435f-b941-f369b21b3919"],
Cell[CellGroupData[{
Cell[807, 29, 234, 4, 91, "Chapter",ExpressionUUID->"9a70a099-afd9-4d76-8ce3-71ea995685a5"],
Cell[1044, 35, 584, 9, 41, "Input",ExpressionUUID->"e5041401-a738-4cb6-8db1-332b0d65ab54"]
}, Open  ]]
}
]
*)

